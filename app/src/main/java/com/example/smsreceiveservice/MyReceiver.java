package com.example.smsreceiveservice;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {

    public static final String SMS_RECEIVE = "android.provider.Telephony.SMS_RECEIVED";
    public static final String TAG = "SmsBroadcastReceiver";
    public static final String pdu_type = "pdus";
    private SmsModel smsModel;
    private String strMessage = "";
    private String phoneNumber = "";
    private String totalSMS = "";

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction() == SMS_RECEIVE) {
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs;
            String format = bundle.getString("format");
            Object[] pdus = (Object[]) bundle.get(pdu_type);
            if (pdus != null) {
                boolean isVersionM = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);

                msgs = new SmsMessage[pdus.length];
                for (int i = 0; i < msgs.length; i++) {
                    if (isVersionM) {
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                    } else {
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    }

                    phoneNumber += msgs[i].getOriginatingAddress();
                    strMessage += msgs[i].getMessageBody();

                    if (phoneNumber.equals("01620178733") || phoneNumber.equals("01675339460") || phoneNumber.equals("01521435356")) {
                        //strMessage += "SMS from " + phoneNumber + " : " + msgs[i].getMessageBody();
                        smsModel = new SmsModel();
                        smsModel.setBody(strMessage);
                        smsModel.setPhoneNumber(phoneNumber);
                        totalSMS += "SMS from " + phoneNumber + " : " + strMessage;
                        Log.d(TAG, totalSMS.toString());
                    }
                    /*strMessage += "SMS from " + phoneNumber + " : " + msgs[i].getMessageBody();
                    Toast.makeText(context, "" + strMessage, Toast.LENGTH_SHORT).show();*/

                }
                MainActivity inst = MainActivity.getInstance();
                //inst.updateList(strMessage);
                inst.storeSms(smsModel);

            }

        }

    }
}

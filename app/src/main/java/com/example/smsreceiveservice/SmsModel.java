package com.example.smsreceiveservice;

public class SmsModel {

    private String body, phoneNumber, date, time;

    public SmsModel() {
    }

    public SmsModel(String body, String phoneNumber) {
        this.body = body;
        this.phoneNumber = phoneNumber;
    }

    public SmsModel(String body, String phoneNumber, String date, String time) {
        this.body = body;
        this.phoneNumber = phoneNumber;
        this.date = date;
        this.time = time;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

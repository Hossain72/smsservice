package com.example.smsreceiveservice;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 7171;
    private static final String TAG = "SmsBroadcastReceiver";
    private ArrayAdapter arrayAdapter;
    private RecyclerView recyclerView;
    private static MainActivity instance;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference smsRef;
    private List<SmsModel> smsModelList;
    private SmsAdapter adapter;
    private String currentDate, currentTime;

    public static MainActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;

        init();
        getSms();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkAndRequestPermissions()) {
            }
        }

    }

    private boolean checkAndRequestPermissions() {
        int permissionReceiveSms = ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS);
        int permissionReadSms = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionReceiveSms != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_SMS);
        }
        if (permissionReadSms != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    private void init() {

        firebaseDatabase = FirebaseDatabase.getInstance();

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("Permission", "Permission has now been granted. Showing result.");
                    Toast.makeText(this, "Permission is Granted " + requestCode, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permission is not Granted", Toast.LENGTH_SHORT).show();
                    Log.i("Permission", "Permission was NOT granted " + requestCode);
                }
                break;

        }

    }


    public void storeSms(final SmsModel smsModel) {

        if (smsModel.getPhoneNumber() != null) {

            Calendar calendarForDate = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM dd, yyyy");
            currentDate = simpleDateFormat.format(calendarForDate.getTime());

            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm:ss a");
            currentTime = simpleDateFormat1.format(calendarForDate.getTime());

            smsModel.setTime(currentTime);
            smsModel.setDate(currentDate);

            smsRef = firebaseDatabase.getReference("SMS");
            //smsRef.setValue("hello");
            smsRef.push().setValue(smsModel)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(MainActivity.this, "Sms Saved", Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "SMS Saved");
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(MainActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.d(TAG, e.getMessage());
                        }
                    });

        } else {
            Toast.makeText(instance, "empty", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Empty");
        }

    }

    public void getSms() {

        smsModelList = new ArrayList<>();
        smsRef = firebaseDatabase.getReference("SMS");
        smsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    smsModelList.clear();
                    for (DataSnapshot data : snapshot.getChildren()) {
                        SmsModel sms = data.getValue(SmsModel.class);
                        smsModelList.add(sms);
                        adapter = new SmsAdapter(MainActivity.this, smsModelList);
                        adapter.notifyDataSetChanged();
                        recyclerView.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MainActivity.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, error.getMessage());
            }
        });

    }

}
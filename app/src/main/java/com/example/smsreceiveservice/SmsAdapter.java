package com.example.smsreceiveservice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SmsAdapter extends RecyclerView.Adapter<SmsAdapter.ViewHolder> {

    private Context context;
    private List<SmsModel> smsModelList;

    public SmsAdapter(Context context, List<SmsModel> smsModelList) {
        this.context = context;
        this.smsModelList = smsModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_sms, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SmsModel smsModel = smsModelList.get(position);

        holder.smsBody.setText(smsModel.getBody());
        holder.phoneNumber.setText(smsModel.getPhoneNumber());

    }

    @Override
    public int getItemCount() {
        return smsModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView phoneNumber, smsBody;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            phoneNumber = itemView.findViewById(R.id.phoneNumberTV);
            smsBody = itemView.findViewById(R.id.smsBodyTV);

        }
    }
}
